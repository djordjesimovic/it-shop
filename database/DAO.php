<?php
require_once 'db.php';

class DAO {
	private $db;

	private $INSERTOSOBA = "INSERT INTO osobe (ime, prezime, JMBG) VALUES (?, ?, ?)";
	private $DELETEOSOBA = "DELETE  FROM osobe WHERE id = ?";
	private $SELECTBYID = "SELECT * FROM osobe WHERE id = ?";	
	private $GETLASTNOSOBA = "SELECT * FROM osobe ORDER BY id DESC LIMIT ?";
	private $SELECTOSOBEBYIME = "SELECT * FROM osobe where ime = ?";

	// private $select_all_products = "SELECT * FROM products";
	private $select_all_products = "SELECT * FROM products JOIN brand ON products.brand_id = brand.brand_id";
	private $select_orders = "SELECT * FROM orders";
	private $order_details = "SELECT oi.order_item_id, oi.amount, p.category, p.brand,p.name, p.color, p.price FROM order_item oi JOIN products p ON p.product_id=oi.product_id WHERE oi.order_id=?";
	private $select_users = "SELECT * FROM users";
	private $insert_user = "INSERT INTO users (username, user_password) VALUES (?, ?)";
	private $select_product_by_id = "SELECT * FROM products JOIN brand ON products.brand_id = brand.brand_id WHERE products.product_id = ?";
	private $insert_order = "INSERT INTO orders(addres, phone_number) VALUES (?, ?)";

	public function __construct()
	{
		$this->db = DB::createInstance();
	}


	public function getAllProducts(){
		$statement = $this->db->prepare($this->select_all_products);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function getOrders(){
		$statement = $this->db->prepare($this->select_orders);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function orderDetails($order_id){
		$statement = $this->db->prepare($this->order_details);
		$statement->bindValue(1, $order_id);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
	public function selectUsers(){
		$statement = $this->db->prepare($this->select_users);
		
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function insertUser($username, $user_password){
		$statement = $this->db->prepare($this->insert_user);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $user_password);

		$statement->execute();
	}

	public function selectProductById($product_id){
		$statement = $this->db->prepare($this->select_product_by_id);
		$statement->bindValue(1, $product_id);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function insertOrder($addres, $phone_number){
		$statement = $this->db->prepare($this->insert_order);
		$statement->bindValue(1, $addres);
		$statement->bindValue(2, $phone_number);

		$statement->execute();
	}
}
?>