<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Page</title>
    <link rel="stylesheet" href="../Landing Pages/homestyle.css">
    <link rel="stylesheet" href="contactstyle.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

</head>
<body>

<?php
  include_once("../Partials/header.php")
?>


<!-- ============== Hero Secion Start ============== -->

  <div class="hero-container">
    <div class="store-information">
      
      <h2>Store Information</h2>

      <div class="store-location">
        <div class="location-icon">
          <i class="bi bi-geo-alt-fill fa-3x"></i>
        </div>
        <div class="location-text">
          <p>Leo Technology</p>
          <p>United States</p>
        </div>
        
      </div>

      <div class="store-mail">
        <div class="mail-icon">
          <i class="bi bi-envelope fa-3x"></i>
        </div>
        <div class="mail-text">
          <p>Email us:</p>
          <a href="">demo@demo.com</a>
        </div>

      </div>

    </div>

    <div class="map-container-form">
      <h2>Our Location</h2>
      <div class="map-section">
        <div id="map"></div>
      </div>
      <h2>Drop Us A Line</h2>
      <p>Have a question or comment? Use the form below to send us a message or contact us by mail</p>


      <div class="container">
        <form action="" method="get" class="form">
      
          <div class="row form-row-first">
            <div class="col-50">
              <select id="customer-service" name="customer-service">
                <option value="australia">Customer Service</option>
                <option value="canada">Excample</option>
                <option value="usa">Excample</option>
              </select>
              <div class="error-message select-error">Error Message</div>
            </div>
              <div class="col-50">
                <input type="text" id="email" name="email"  placeholder="Your@email.com" >
                <div class="error-message mail-error">Error Message</div>
              </div>
            
          </div>

          <div class="row file">

            <div class="col-75">
              <input type="file" id="file-chose">
            </div>
            <div class="col-25">
              <label for="file-chose">Optional</label>
            </div>

          </div>

          <div class="row">
            <div class="col-100">
              <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>
            </div>
            <div class="error-message text-error">Error Message</div>
          </div>
          <br>
          <div class="row">
            <button type="submit" id="post-comment">Post comment </button>
          </div>
        </form>
      </div>
    </div>
  </div>

<!-- ============== Hero Section End ============== -->

<?php
  include_once('../Partials/footer.php');
?>


<script src="../Contact Page/map.js"></script>
<script src="../Landing Pages/homescript.js"></script>
<script src="contactscript.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap&v=weekly"></script>
<script src="../partials/partialScript.js"></script>
</body>
</html>