<!-- ============== Subscribe Section Start ============== -->

<div class="subscribe-container">
    <div class="message">
      <div class="message-icon">
        <i class="bi bi-envelope fa-3x"></i>
      </div>
      <div class="message-text">
        <h2>Fear of missing out?</h2>
        <p>Get the leatest deals, updates and more</p>
      </div>
    </div>

    <div class="subscribe-form">
      <form action="">
        <input type="email" name="email" id="mail-input" placeholder="Your email addres " width="100%">
        <input type="submit" name="submit-button" id="submit">
      </form>
      
    </div>

    <div class="social-media">
      <div class="social-media-text">
        <p>Follow us</p>
      </div>
      <div class="social-media-icons">
        <a href=""><i class="bi bi-facebook fa-2x"></i></a>
        <a href=""><i class="bi bi-twitter fa-2x"></i></a>
        <a href=""><i class="bi bi-youtube fa-2x"></i></a>
        <a href=""><i class="bi bi-instagram fa-2x"></i></a>
      </div>
    </div>
  </div>

<!-- ==============Subscribe Section End ============== -->


<!-- ==============Contact And Info Section Start ============== -->

<div class="informations">
        <ul class="info">
            <li class="item first">Company</li>
            <li class="item"><a href="">About Us</a></li>
            <li class="item"><a href="">Careers</a></li>
            <li class="item"><a href="">Affiliates</a></li>  
            <li class="item"><a href="">Blog</a></li>
            <li class="item"><a href="">Contact Us</a></li>
        </ul>

        <ul class="info">
            <li class="item first">Shop</li>
            <li class="item"><a href="">Televisions</a></li>
            <li class="item"><a href="">Fridges</a></li>
            <li class="item"><a href="">Washing Machines</a></li>
            <li class="item"><a href="">Fans</a></li>
            <li class="item"><a href="">Air Conditions</a></li>
            <li class="item"><a href="">Laptops</a></li>
        </ul>

        <ul class="info">
            <li class="item first">Help</li>
            <li class="item"><a href="">Customer Services</a></li>
            <li class="item"><a href="">My Accounts</a></li>
            <li class="item"><a href="">Find a Store</a></li>
            <li class="item"><a href="">Legal & Privacy</a></li>
            <li class="item"><a href="">Contack</a></li>
            <li class="item"><a href="">Gift Card</a></li>
        </ul>


        <ul class="info">
            <li class="item first"><a href="">My Account</a></li>
            <li class="item"><a href="">My Profile</a></li>
            <li class="item"><a href="">My Order History</a></li>
            <li class="item"><a href="">My Wish List</a></li>
            <li class="item"><a href="">Order Tracking</a></li>
        </ul>

        <ul class="info">
            <li class="item first"><a href="">Contact Info</a></li>
            <li class="item"><a href=""><h4>(+84)-01234-5678</h4></a></li>
            <li class="item"><p>Mon To Sun: 10:00 AM To 6:00 PM</p></li>
            <li class="item"><a href="" class="mail-contact">demo@demo.com</a></li>
            <li class="item"><a href="">48 West Temple Drive</a></li>
            <li class="item"><a href="">Ashburn, VA 20147</a></li>
        </ul>

    </div>

<!-- ============== Contact And Info Section End ============== -->


<!-- ============== Guarantee Section Start ============== -->

    <div class="guarantee-section-container">

      <div class="guarantee-article">

        <div class="guarantee-icon">
          <i class="bi bi-truck fa-3x"></i>
        </div>
        <div class="guarantee-text">
          <h4>Fast Delivery</h4>
          <p>For All Orders Over $120</p>
        </div>

      </div>


      <div class="guarantee-article">

        <div class="guarantee-icon">
          <i class="bi bi-wallet fa-3x"></i>
        </div>
        <div class="guarantee-text">
          <h4>Free Return</h4>
          <p>Free Return Over $100</p>
        </div>

      </div>


      <div class="guarantee-article">

        <div class="guarantee-icon fa-3x">
          <i class="bi bi-headset"></i>
        </div>
        <div class="guarantee-text">
          <h4>Customer Support</h4>
          <p>Friendly 24/7 Customer Support</p>
        </div>
      </div>


      <div class="guarantee-article">

        <div class="guarantee-icon fa-3x">
          <i class="bi bi-patch-check"></i>
        </div>
        <div class="guarantee-text">
          <h4>Money Back Guarantee</h4>
          <p>Quality Checked By Our Team</p>
        </div>

      </div>

      
    </div>

<!-- ============== Guarantee Section End ============== -->


<!-- ============== Footer Start ============== -->

    <footer class="foot">
      <div class="left-footer">
        <span>Copyright © 2022 Technology. All rights reservd</span>
      </div>

      <div class="right-footer">
        <ul>
          <a href=""><li>Privacy Policy</li></a>
          <a href=""><li>Terms & Conditions</li></a>
          <a href=""><li>Cookie</li></a>
          <li><img src="../Landing Pages/images/visa.png" alt="" width="30px" height="10px"></li>
          <li><img src="../Landing Pages/images/mastercard.png" alt="" width="40px" height="40px"></li>
          <li><img src="../Landing Pages/images/paypal.png" alt="" width="50px" height="60px"></li>
          <li><img src="../Landing Pages/images/amazon.png" alt="" width="60px" height="30px"></li>
        </ul>
      </div>
    </footer>

<!-- ============== Footer End ============== -->