<?php

require_once '../database/DAO.php';


    $username = isset($_GET['username']) ? $_GET['username'] : "";
    $user_password = isset($_GET['user_password']) ? $_GET['user_password'] : "";
    $repassword = isset($_GET['rePassword']) ? $_GET['rePassword'] : "";

    $errors = [];

    $dao = new DAO();

    $allUsers = $dao -> selectUsers();
    // var_dump($allUsers);

    if($username == "" || $user_password == "" || $repassword == ""){
        echo($errors['emptyField'] = 'All fields are required');
        header('location: registrationIndex.php');
    }
    elseif(strlen($username) < 5){
        echo($errors['username'] = 'Username must be 5 or more characters long');
        header('location: registrationIndex.php');
    }
    elseif(strlen($user_password) < 5){
       echo($errors['password'] = 'Password must be 5 or more characters long');
        header('location: registrationIndex.php');
    }
    elseif($user_password !== $repassword){
        echo($errors['matchingPass'] = 'Passwords must be the same');
        header('location: registrationIndex.php');
    }
    else{
        $insertUsers = $dao -> insertUser($username, $user_password);
    }

    

    // if($_SERVER['REQUEST_METHOD'] == 'GET'){
    //     switch($action){
    //         case 'Register':
    //         $dao = new DAO();
    //         $dao -> insertUser($username, $user_password);
    //         echo("Registered");
    //         break;

    //         default:
    //         echo('Error');
    //         break;
    //     }
    // }

    // $errors = [];

    // if(strlen($username) < 5){
    //     // header('location: registrationIndex.php');
    //     $errors['username'] = 'Username must have 5 or more characters';
    //     var_dump($errors);
    // }
    // else{
    //     $dao = new DAO();
    //     $insertUsers = $dao -> insertUser($username, $user_password);
    // }
    
?>