<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Orders</title>
    <link rel="stylesheet" href="../Shop Page/shopstyle.css">
     <link rel="stylesheet" href="../Landing Pages/homestyle.css">
     <link rel="stylesheet" href="../Contact Page/contactstyle.css">
     <link rel="stylesheet" href="ordersStyle.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
    <?php include_once('../Partials/header.php');?>

    <div class="orders-container">
            <div class="product-order-headings">
                <div class="order-id-head">OrderID</div>
                <div class="order-time-head">Order Time</div>
                <div class="addres-head">Order Addres</div>
                <div class="phone-number-head">Phone Number</div>
                <div class="order-details">Order Details</div>
            </div>
        <template class="product-order-template">
            <div class="product-order-card">
                <div class="order-id"></div>
                <div class="order-time"></div>
                <div class="addres"></div>
                <div class="phone-number"></div>
                <button class="detail-btn">Show details</button>
            </div>
        </template>

        <div class="popup-modal">
            <button id="closeModal">X</button>

            <h4>Order Details</h4>
            <table>
                <tr class="order-head">
                    <th>Brand</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Amount</th>
                    <th>Price</th>
                </tr>
                <tr>
                    <td class="order-brand">Test</td>
                    <td class="order-name">Test</td>
                    <td class="order-color">Test</td>
                    <td class="amount">Test</td>
                    <td class="order-price">Test</td>
                </tr>
            </table>
        </div>

        <div class="overlay"></div>

    </div>

    <?php include_once('../Partials/footer.php');?>

    <script src="ordersScript.js"></script>
    <script src="../partials/partialScript.js"></script>
</body>
</html>