
<header class="head"> 

<a href="#" class="toggle-button">
  <span class="bar"></span>
  <span class="bar"></span>
  <span class="bar"></span>
</a>

<ul class="nav navbar-left">
    <li><a href="#" class="nav-link">Shipping</a></li>
    <li><a href="#" class="nav-link">FAQ</a></li>
    <li><a href="#" class="nav-link">Contact</a></li>
    <li><a href="#" class="nav-link">Track Order</a></li>
</ul>

<!-- <div class="nav-bar-container"> -->

<ul class="nav navbar-right">
  <!-- <li><a href="#" class="nav-link"> Free 30 Day Money Back Guarantee</a></li> -->
  <li><a href="#" class="nav-link">USD</a></li>
  <li><a href="#" class="nav-link">English</a></li>
</ul>

<!-- </div> -->


</header>





<!-- ============== First Section Start ============== -->

<section class="firstSection">

<div class="firstLeft">

    <div class="logo">
        <img src="../Landing Pages/images/logo.png" alt="" width="292" height="104">
    </div>

    <div class="logo-text">
      <div class="mail">
          <p>Send us a message</p>
          <a href="">demo@demo.com</a>
      </div>
      <div class="contact">
          <p>Need help? Call Us:</p>
          <h5>012 345 6789</h5>
      </div>
    </div>

</div>

<div class="firstRight">
    <a href="../Registration Page/registrationIndex.php"><i class="bi bi-person-fill"></i></a>
    <a href=""><i class="bi bi-heart"></i></a>
    <a href="" class="bag">
        <i class="bi bi-bag"></i>
        <div class="shopping-card">
            <template class="shopping-card-template">
                <div class="card-heading">
                    Card
                </div>
                <div class="ordered-product">
                    <div class="order-name"></div>
                    <div class="amounth">+</div>
                    <div class="amounth">-</div>
                </div>
            </template>
        </div>
    </a>
</div>

</section>
<!-- ============== First Section End ============== -->


<!-- ============== Second Section Start ============== -->
<section class="secondSection">
    <div class="dropDown">
        <button>Browse Categories</button>
        <ul class="menu-navigation">
            <li><a href="../Landing Pages/homeindex.php">Home</a></li>
            <li><a href="../Shop Page/shopindex.php">Shop</a></li>
            <li><a href="">Pages</a></li>
            <li><a href="">Blogs</a></li>
            <li><a href="../Contact Page/contact.php">Contact</a></li>
            <li><a href="../Login Page/login.php">Orders</a></li>
        </ul>
    </div>
    <div class="searchbar">
        <!-- Search form -->
        <div class="search-form">
            <input class="form-control" type="text" placeholder="Search" aria-label="Search">
            <a href=""><i class="bi bi-search"></i></a>
        </div>
    </div>
</section>
<!-- ============== Second Section End ============== -->

<script src="../Landing Pages/homescript.js"></script>