<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registration</title>
     <link rel="stylesheet" href="../Landing Pages/homestyle.css">
     <link rel="stylesheet" href="regStyle.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
    <?php include_once('../Partials/header.php');?>

    <div class="registration-form-container">
    <h2>Register</h2>
        <form action="registrationController.php" method="GET" class="registration-form">
            <div class="username-input reg-input">
                <input type="text" name="username" placeholder="Insert Username" usernameField>
                <div class="error-msg" usernameError>Error Message</div>
            </div>
            <div class="password-input reg-input">
                <input type="password" name="user_password" placeholder="Insert Password" password>
                <div class="error-msg" passwordError>Error Message</div>
            </div>
            <div class="password-retype-input reg-input">
                <input type="password" name="rePassword" placeholder="Retype Password" passwordRetype>
                <div class="error-msg" retypePasswordError>Error Message</div>
            </div>
            <input type="submit" value="Register" id="register"  registerButton>
            <p>Alredy have account?</p>
            <a href="../Login Page/login.php">Login</a>
        </form>
    </div>


    <?php include_once('../Partials/footer.php');?>

    <script src="registrationScript.js"></script>
    <script src="../partials/partialScript.js"></script>
</body>
</html>