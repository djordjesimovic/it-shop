<?php

    $mail = isset($_GET["email"]) ? $_GET["email"] : '';
    $text = isset($_GET["subject"]) ? $_GET["subject"] : '';

    $emptyMail = $mail == '';
    $validMailFormat = filter_var($mail, FILTER_VALIDATE_EMAIL);

    $emptyText = $text == '';
    
    $myJSON = json_encode([
        'emptyMail' => $emptyMail,
        'mailFormat' => $validMailFormat,
        'emptyText' => $emptyText
    ]);
    echo $myJSON;

?>