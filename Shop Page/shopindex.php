<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop</title>
    <link rel="stylesheet" href="shopstyle.css">
     <link rel="stylesheet" href="../Landing Pages/homestyle.css">
     <link rel="stylesheet" href="../Contact Page/contactstyle.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="../Orders/ordersStyle.css">
    <link rel="stylesheet" href="detailsStyle.css">
</head>
<body>

<?php
  include_once('../Partials/header.php');
?>

<!-- ============== Hero Section Start ============== -->

  <div class="hero-shop-container">
      <div class="categories-container">
          <div class="home-box">
              <h2>Home</h2>
              <div class="home-filter">
                  <button class="filter-btn" id="laptop-btn">Laptops</button>
                  <button class="filter-btn" id="television-btn">Television</button>
                  <button class="filter-btn" id="phone-btn">Phones</button>
                  <button class="filter-btn" id="all-btn">All</button>
              </div>
          </div>
          <div class="categories-box home-box">
              <h2>Categories</h2>
              <div class="home-filter checkbox-filter" category-filter>
                  <label><input type="checkbox" value="Laptops" name="laptops" id="laptops">Laptops</label>
                  <label><input type="checkbox" value="Television" name="tv" id="tv">Television</label>
                  <label><input type="checkbox" value="Phones" name="phones" id="phones">Phones</label>
              </div>
          </div>

          <div class="color-box home-box">
              <h2>Color</h2>
              <div class="home-filter checkbox-filter" color-filter>
                  <label><input type="checkbox" value="black" name="black">Black</label>
                  <label><input type="checkbox" value="white" name="white">White</label>
              </div>
          </div>

          <div class="brand-box home-box">
            <h2>Brand</h2>
            <div class="home-filter checkbox-filter" brand-filter>
                <label><input type="checkbox" value="samsung" name="samsung">Samsung</label>
                <label><input type="checkbox" value="hp" name="hp">HP</label>
                <label><input type="checkbox" value="lg" name="lg">LG</label>
                <label><input type="checkbox" value="lenovo" name="lenovo">Lenovo</label>
            </div>
        </div>

        <div class="price-box home-box">
          <h2>Price</h2>

          <div class="price-input">
            <div class="field">
              <span>$</span>
              <input type="number" class="input-min" value="150">
            </div>
            <div class="separator">-</div>
            <div class="field">
              <span>$</span>
              <input type="number" class="input-max" value="750">
            </div>
          </div>
          <div class="slider">
            <div class="progress"></div>
          </div>
          <div class="range-input">
            <input type="range" class="range-min" min="0" max="1000" value="150" step="10">
            <input type="range" class="range-max" min="0" max="1000" value="750" step="10">
          </div>

        </div>

      </div>

      <div class="products-container"  shop-product-container>

        <template class="shop-template" shop-product-template>
          <div class="product-card">
            <div class="image-logo" product-img></div>
            <div class="category">
              <h4 category-type></h4>
              <h4 brand-name></h4>
              <p product-name></p>
              <div class="zvezdice" product-rating></div>
              <h5 product-price></h5>
            </div>
            <div class="over-text">
              <a href="#" class="detaiButton" detailBtn>See More</a>
              <!-- <button class="add-to-card">Add To Card</button> -->
            </div>
          </div>
        </template>

        <div class="popup-order-modal">
          <button id="closeModal">X</button>
          <h4>Product Details</h4>
          <div class="product-details">
            <div class="image-product"></div>
            <div class="name-product"></div>
            <div class="brand-product"></div>
            <div class="color-product"></div>
            <div class="price-product"></div>
          </div>
          <form action="orderController.php" class="order-form">
            <h3>Order Form</h3>
            <input type="text" name="order_id" id='orderID' disabled>
            <input type="text" name="addres" placeholder="Order Addres" id="addres">
            <input type="text" name="phone_number" placeholder="Phone Number" id="phoneNumber">
            <input type="submit" value="Order" class="order-button">
          </form>
          <!-- <a href="#" class="order-button">Order</a> -->
          
        </div>

        <div class="overlay"></div>
        
      </div>
  </div>    

<!-- ============== Hero Section End ============== -->

<?php
  include_once('../Partials/footer.php');
?>

<?php
    require_once '../database/DAO.php';

    $dao = new DAO();

    $produkti = [$dao -> getAllProducts()];
    
  ?>


  <script src="shopscript2.js"></script>
  <script src="../Landing Pages/homescript.js"></script>
  <script src="../partials/partialScript.js"></script>
</body>
</html>
