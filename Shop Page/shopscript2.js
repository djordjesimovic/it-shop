const shopProductTemplate = document.querySelector("[shop-product-template]");
const shopProductContainer = document.querySelector("[shop-product-container]");
const searchInput = document.querySelector(".searchbar .search-form .form-control");

let filterButtons = document.querySelectorAll('.filter-btn');
let categoryFilter = document.querySelectorAll("[category-filter]");
let colorFilter = document.querySelectorAll("[color-filter]");
let brandFilter = document.querySelectorAll("[brand-filter]");

let modalWindow = document.querySelector('.popup-order-modal');
let overlay = document.querySelector('.overlay');
let closeModalBtn = document.querySelector('#closeModal');

let shoppingCard = document.querySelector('.shopping-card');



const request = new XMLHttpRequest();
var sendIDRequest = new XMLHttpRequest();
const sendOrderRequest = new XMLHttpRequest();

sendIDRequest.onload = function(){
    let response = JSON.parse(sendIDRequest.responseText);
    productDetails = response.productById;
    console.log(productDetails);

    overlay.style.display = 'block';
    modalWindow.style.display = 'block';

    const productPhoto = document.querySelector('.image-product');
    const productName = document.querySelector('.name-product');
    const productBrand = document.querySelector('.brand-product');
    const productColor = document.querySelector('.color-product');
    const productPrice = document.querySelector('.price-product');
    const orderId = document.querySelector('#orderID');
    const orderAddres = document.querySelector('#orderAddres');
    const orderPhone = document.querySelector('#phoneNumber');
    let orderButtons = document.querySelectorAll('.order-button');


    productPhoto.innerHTML = `<img src="../Landing Pages/images/${productDetails.photo_dir}" width="160" height="180"></img>`
    productName.textContent = productDetails.name;
    productBrand.textContent = productDetails.brand;
    productColor.textContent = productDetails.color;
    productPrice.textContent = `$${productDetails.price}`;
    orderId.value  = productDetails.name;

    closeModalBtn.addEventListener('click', () =>{
        overlay.style.display = 'none';
        modalWindow.style.display = 'none';
    });

    for (const button of orderButtons) {
        button.addEventListener('click', e =>{
            // e.preventDefault();
            console.log(button);
            sendOrderRequest.open('get', `orderController.php?addres=${orderAddres.value}&phone_number=${orderPhone.value}`);
        })
    }

}

request.onload = function(){

    let products = [];

    let object = JSON.parse(request.responseText);

    let productsList = object.AllProducts;


    products = productsList.map(product => {
        const card = shopProductTemplate.content.cloneNode(true).children[0];
        const category = card.querySelector('[category-type]');
        const brand = card.querySelector('[brand-name]');
        const name = card.querySelector('[product-name]');
        const price = card.querySelector('[product-price]');
        const productImg = card.querySelector('[product-img]');
        const productRating = card.querySelector("[product-rating]");
        const detailBtns = card.querySelector("[detailBtn]");


        category.textContent = product.category;
        brand.textContent = product.brand
        name.textContent = product.name;
        price.textContent = `$${product.price}`;
        productImg.innerHTML = `<img src="../Landing Pages/images/${product.photo_dir}" width="160" height="180"></img>`;
        productRating.innerHTML = '<i class="fal fa-star"></i> <i class="fal fa-star"></i> <i class="fal fa-star"></i> <i class="fal fa-star"></i><i class="fal fa-star"></i>';
        shopProductContainer.append(card);

        
        return{
            product_id: product.product_id,
            category: product.category,
            name: product.name,
            price: product.price,
            color: product.color,
            productImg: product.photo_dir,
            brand: product.brand_name,
            detailBtn: detailBtns,
            element: card
        }
    });

    console.log(products);

    let productCardTemplate = document.querySelector('.shopping-card-template')

    for (const product of products) {
        product.detailBtn.addEventListener('click', (e) => {
            e.preventDefault();
            sendIDRequest.open('get', 'products.php?product_id=' + product.product_id);
            sendIDRequest.send();




            // const shoppingCard = productCardTemplate.content.cloneNode(true).children[0];
            // const name = shoppingCard.querySelector('.order-name');

            // console.log(name)
            // name.innerText = product.name;

            // let order = document.createElement('div');
            // order.innerText = `${product.brand} ${product.name}`;
            // order.classList.add('ordered-product');
            // let plus = document.createElement('button');
            // plus.innerText = '+';
            // plus.classList.add('amounth');
            // order.appendChild(plus);
            // let minus = document.createElement('button');
            // minus.innerText = '-';
            // minus.classList.add('amounth');
            // order.appendChild(minus);
            // shoppingCard.appendChild(order);
        })
    }

    // =============== Search Bar =================
    searchInput.addEventListener('input', (e) => {
        const value = e.target.value.toLowerCase();
        products.forEach(product => {
            const isVisible = product.category.toLowerCase().includes(value) || product.name.toLowerCase().includes(value) || product.brand.toLowerCase().includes(value);
            product.element.classList.toggle('hide', !isVisible)
        })
    })

    // =============== Buttons Filter =================
    for (const btn of filterButtons) {
        btn.addEventListener('click', (e) => {
            products.forEach(product => {
                if(e.target.innerText === 'All'){
                    product.element.classList.remove('hide')
                }
                else if(e.target.innerText.toLowerCase() != product.category){
                    product.element.classList.add('hide');
                }
                else{
                    product.element.classList.remove('hide');
                }
            })
        })
    }

    // =============== Category CheckBox Filter =================
    for (const categoryCheckBox of categoryFilter) {
        categoryCheckBox.addEventListener('change', (e) => {
            if(e.target.checked){
                products.forEach(product => {
                    if(e.target.value.toLowerCase() === product.category.toLowerCase()){
                        product.element.classList.remove('hide');
                    }
                })
            }
            else{
                products.forEach(product => {
                    if(e.target.value.toLowerCase() === product.category.toLowerCase()){
                        product.element.classList.add('hide');
                    }
                })
            }
        })
    }

    // =============== Color CheckBox Filter =================
    for (const colorCheckBox of colorFilter) {
        colorCheckBox.addEventListener('change', (e) => {
            if(e.target.checked){
                products.forEach(product => {
                    if(e.target.value.toLowerCase() === product.color.toLowerCase()){
                        product.element.classList.remove('hide');
                    }
                })
            }
            else{
                products.forEach(product => {
                    if(e.target.value.toLowerCase() === product.color.toLowerCase()){
                        product.element.classList.add('hide');
                    }
                })
            }
        })
    }

    // =============== Brand CheckBox Filter =================
    for (const brandCheckBox of brandFilter) {
        brandCheckBox.addEventListener('change', (e) => {
            if(e.target.checked){
                products.forEach(product => {
                    if(e.target.value.toLowerCase() === product.brand.toLowerCase()){
                        product.element.classList.remove('hide');
                    }
                })
            }
            else{
                products.forEach(product => {
                    if(e.target.value === product.brand.toLowerCase()){
                        product.element.classList.add('hide');
                    }
                })
            }
        })
    }

    // =============== Price Range Filter =================
    const rangeInput = document.querySelectorAll(".range-input input"),
    priceInput = document.querySelectorAll(".price-input input"),
    range = document.querySelector(".slider .progress");
    let priceGap = 100;

    priceInput.forEach(input =>{
        input.addEventListener("input", e =>{
            let minPrice = parseInt(priceInput[0].value),
            maxPrice = parseInt(priceInput[1].value);
            
            if((maxPrice - minPrice >= priceGap) && maxPrice <= rangeInput[1].max){
                if(e.target.className === "input-min"){
                    rangeInput[0].value = minPrice;
                    range.style.left = ((minPrice / rangeInput[0].max) * 100) + "%";
                }else{
                    rangeInput[1].value = maxPrice;
                    range.style.right = 100 - (maxPrice / rangeInput[1].max) * 100 + "%";
                }
            }
            products.forEach(product => {
                if(!(product.price >= priceInput[0].value && product.price <= priceInput[1].value)){
                    product.element.classList.add('hide');
                }
                else{
                    product.element.classList.remove('hide');
                }
            })
        });
    });

    rangeInput.forEach(input =>{
        input.addEventListener("input", e =>{
            let minVal = parseInt(rangeInput[0].value),
            maxVal = parseInt(rangeInput[1].value);
            if((maxVal - minVal) < priceGap){
                if(e.target.className === "range-min"){
                    rangeInput[0].value = maxVal - priceGap
                }else{
                    rangeInput[1].value = minVal + priceGap;
                }
            }else{
                priceInput[0].value = minVal;
                priceInput[1].value = maxVal;
                range.style.left = ((minVal / rangeInput[0].max) * 100) + "%";
                range.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
            }
    
            products.forEach(product => {
                if(!(product.price >= rangeInput[0].value && product.price <= rangeInput[1].value)){
                    product.element.classList.add('hide');
                }
                else{
                    product.element.classList.remove('hide');
                }
            })
        });
    });

}
request.open('get','products.php');
request.send();
