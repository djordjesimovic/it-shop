<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <link rel="stylesheet" href="homestyle.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>


<?php
  include_once("../Partials/header.php")
?>

<!-- ============== Hero Section Start ============== -->

    <div class="lightbox">
        <div class="row">
          <div class="col-md-6">
            <div class="photogroup">
              <img
                src="images/headphones.jpeg"
                data-mdb-img="https://mdbcdn.b-cdn.net/img/Photos/Slides/1.webp"
                alt="Table Full of Spices"
                class="w-100 mb-2 mb-md-4 shadow-1-strong rounded"
              />
              <div class="overText">
                <a href="" class="detailInfo">Discover</a>
                <h1>Beat Pro X3</h1>
                <p>$259.99 $199.99</p>
                <button class="shop">Shop Now</button>
              </div>
            </div>
            <div class="photogroup">
              <img
                src="images/macbook.webp"
                data-mdb-img="https://mdbcdn.b-cdn.net/img/Photos/Square/1.webp"
                alt="Coconut with Strawberries"
                class="w-100 shadow-1-strong rounded"
              />
              <div class="overText">
                <a href="" class="detailInfo">Limited Stock</a>
                <h1>Macbook Air</h1>
                <p>Starting From $900.99</p>
                <button class="shop">Shop Now</button>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="photogroup">
              <img
                src="images/phone3.jpg"
                data-mdb-img="https://mdbcdn.b-cdn.net/img/Photos/Vertical/1.webp"
                alt="Dark Roast Iced Coffee"
                class="w-100 shadow-1-strong rounded"
              />
              <div class="overText">
                <a href="" class="detailInfo">MIdweek Details</a>
                <h1>Smartphone <br> Accessories</h1>
                <p>Up to 40% off</p>
                <button class="shop">Shop Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
<!-- ============== Hero Section End ============== -->


<!-- ============== Featured Offers Section Start ============== -->

    <div class="naslov">
      <h1>Our Featured Offers</h1>
      <a href="">View All</a>
    </div>
    <br>

    <div class="artikli">
      <div class="product">
        <div class="imageLogo">
          <img src="images/tlefon.jpg" alt="" width="160" height="180">
        </div>
        <div class="category">
          <h4>Tablets</h4>
          <p>Samsung Galaxy M51</p>
          <div class="zvezdice">
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
          </div>
          <h5>$19.12</h5>
        </div>
      </div>

      <div class="product">
        <div class="imageLogo">
          <img src="images/laptop.webp" alt="" width="200" height="180">
        </div>
        <div class="category">
          <h4>Tablets</h4>
          <p>Lenovo IdealPad S540 13.3"</p>
          <div class="zvezdice">
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
          </div>
          <h5>$28.72</h5>
        </div>
      </div>

      <div class="product">
        <div class="imageLogo">
          <img src="images/tv.webp" alt="" width="230" height="180">
        </div>
        <div class="category">
          <h4>Tablets</h4>
          <p>TV LED LG Full HD 43 Inch 43LK5000</p>
          <div class="zvezdice">
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
          </div>
          <h5>$27.55</h5>
        </div>
      </div>

      <div class="product">
        <div class="image-logo">
          <img src="images/hplaptop.jpg" alt="" width="160" height="180">
        </div>
        <div class="category">
          <h4>Tablets</h4>
          <p>HP Pavilon X360 Convertblue 14-Dy0064TU</p>
          <div class="zvezdice">
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
            <i class="fal fa-star"></i>
          </div>
          <h5>$29.00</h5>
        </div>
      </div>
    </div>
<!-- ============== Feautured Ofers Section End ============== -->

<!-- ============== This Week Details Section Start ============== -->

  <div class="naslov">
    <h1>This Week Details</h1>
    <a href="">View All Details</a>
  </div>
  <br>


  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="this-week-product">
          <div class="image-logo">
            <img src="images/tlefon.jpg" alt="" width="160" height="180">
          </div>
          <div class="category">
            <h4>Tablets</h4>
            <p>Samsung Galaxy M51</p>
            <div class="zvezdice">
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
            </div>
            <h5>$29.00</h5>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="this-week-product">
          <div class="image-logo">
            <img src="images/hplaptop.jpg" alt="" width="160" height="180">
          </div>
          <div class="category">
            <h4>Mobiles</h4>
            <p>HP Pavilon X360 Convertblue 14-Dy0064TU</p>
            <div class="zvezdice">
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
              <i class="fal fa-star"></i>
            </div>
            <h5>$29.00</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
<!-- ============== This Week Details Section End ============== -->

<!-- ============== Partner Baners Section Start ============== -->
<div class="saradnici">
  <div class="slide first">
      <img alt="Picture for manufacturer Divoom" 
      src="https://www.sampro.rs/images/thumbs/0002419_divoom_250.jpeg" 
      title="Show products manufactured by Divoom">
  </div>

  <div class="slide">
      <img alt="Picture for manufacturer Asus" 
      src="https://www.sampro.rs/images/thumbs/0002414_asus_250.jpeg" 
      title="Show products manufactured by Asus">
  </div>

  <div class="slide">
      <img alt="Picture for manufacturer LG" 
      src="https://www.sampro.rs/images/thumbs/0002432_lg_250.jpeg" 
      title="Show products manufactured by LG">
  </div>

  <div class="slide">
      <img alt="Picture for manufacturer Acer" 
      src="https://www.sampro.rs/images/thumbs/0002411_acer_250.jpeg" 
      title="Show products manufactured by Acer">
  </div>

  <div class="slide">
      <img alt="Picture for manufacturer Kasda" 
      src="https://www.sampro.rs/images/thumbs/0002429_kasda_250.jpeg" 
      title="Show products manufactured by Kasda">
  </div>

  <div class="slide">
      <img alt="Picture for manufacturer Netis" 
      src="https://www.sampro.rs/images/thumbs/0002440_netis_250.jpeg" 
      title="Show products manufactured by Netis">
  </div>

</div>
<!-- ============== Partner Baners Section End ============== -->

<?php
  include_once('../Partials/footer.php');
?>




<script src="homescript.js"></script>

    <!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="../partials/partialScript.js"></script>
</body>
</html>