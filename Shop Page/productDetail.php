<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Detail</title>
    <link rel="stylesheet" href="shopstyle.css">
    <link rel="stylesheet" href="../Landing Pages/homestyle.css">
    <link rel="stylesheet" href="../Contact Page/contactstyle.css">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
   integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
   integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>
    <?php
        include_once('../Partials/header.php')
    ?>

    <!-- ===============Hero Section Start=============== -->
        <!-- <template class="product-detail-container">
            <div class="product-images"></div>
            <div class="product-info">
                <h2 product-name></h2>
            </div>
        </template> -->
        <button clickBtn>Click</button>
    <!-- ===============Hero Section End=============== -->

    <?php
        include_once('../Partials/footer.php')
    ?>


<script src="productDetailScript.js"></script>
</body>
</html>