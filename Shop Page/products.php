<?php

require_once '../database/DAO.php';

    $dao = new DAO();

    if(isset($_GET['product_id'])){
        $product_id = $_GET['product_id'];
        $productById = $dao -> selectProductById($product_id);
    }
    else{
        $productById = [];
    }



    $productsList = $dao -> getAllProducts();



    $object = [
        'AllProducts' => $productsList,
        'productById' => $productById
    ];

    echo json_encode($object);

?>