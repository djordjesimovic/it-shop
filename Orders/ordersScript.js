const ordersContainer = document.querySelector('.orders-container');
const ordersTemplate = document.querySelector('.product-order-template');

let modalWindow = document.querySelector('.popup-modal');
let overlay = document.querySelector('.overlay');
let closeModalBtn = document.querySelector('#closeModal');




const request = new XMLHttpRequest();

request.onload = function(){

  orders = [];

  let object = JSON.parse(request.responseText);
  let ordersList = object.Orders;


  orders = ordersList.map(order =>{
    const orderCard = ordersTemplate.content.cloneNode(true).children[0];
    const orderID = orderCard.querySelector('.order-id');
    const orderTime = orderCard.querySelector('.order-time');
    const addres = orderCard.querySelector('.addres');
    const phoneNumber = orderCard.querySelector('.phone-number');
    let detailBtn = orderCard.querySelectorAll('.detail-btn');


    orderID.textContent = order.order_id;
    orderTime.textContent = order.order_time;
    addres.textContent = order.addres;
    phoneNumber.textContent = order.phone_number;


    for (const btn of detailBtn) {
      btn.addEventListener('click', () =>{
        let requestDetails = fetch(`ordersCatch.php?order_id=${order.order_id}`).then(response => response.json()).then(data =>{
          let orderBrand = document.querySelector('.order-brand');
          let orderProductName = document.querySelector('.order-name');
          let orderProductColor = document.querySelector('.order-color');
          let amount = document.querySelector('.amount');
          let orderPrice = document.querySelector('.order-price');
          let orderDetail = data.OrderDetails;

          for (const detail of orderDetail) {
            overlay.style.display = 'block';
            modalWindow.style.display = 'block';
            orderBrand.innerText = detail.brand;
            orderProductName.innerText = detail.name;
            orderProductColor.innerText = detail.color;
            amount.innerText = detail.amount;
            orderPrice.innerText = `$${detail.price*detail.amount}`;
          }
          closeModalBtn.addEventListener('click', () =>{
            overlay.style.display = 'none';
            modalWindow.style.display = 'none';
          });
        })
      })
    }

    ordersContainer.append(orderCard);

    return{
      orderID: order.order_id,
      orderTime: order.order_time,
      addres: order.order_time,
      phoneNumber: order.phone_number,
      element: orderCard
    }

  })


  console.log(orders)
}

request.open('get','ordersCatch.php');
request.send();