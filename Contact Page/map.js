// Initialize and add the map
// Initialize and add the map
function initMap() {
    // The location of Uluru
    var uluru = { lat: 12, lng: 20.55 };
    // The map, centered at Uluru
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: uluru,
    });
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
    });
  }
  

//   function myMap() {
//     var mapProp= {
//       center:new google.maps.LatLng(51.508742,-0.120850),
//       zoom:5,
//     };
//     var map = new google.maps.Map(document.getElementById("map"),mapProp);
//     }