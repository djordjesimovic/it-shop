let shopingCard = document.querySelector('.bi-bag');
let toggleBtn = document.querySelector('.toggle-button');
let menu = document.querySelector('.navbar-left');

let cardContainer = document.querySelector('.shopping-card');

shopingCard.addEventListener('click', (e) =>{
    e.preventDefault();
    console.log(shopingCard);
    cardContainer.classList.toggle('active');
});

toggleBtn.addEventListener('click', () => {
    menu.classList.toggle('active');
});
