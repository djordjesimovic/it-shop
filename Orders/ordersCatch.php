<?php

require_once '../database/DAO.php';

    $dao = new DAO();

    $ordersList = $dao -> getOrders();

    if (isset($_GET['order_id'])) {
        //var_dump($_GET['order_id']);
        $order_id = $_GET['order_id'];
        $orderDetail = $dao -> orderDetails($order_id);
    } else {
        $orderDetail = [];
    }


    $object = [
        'Orders' => $ordersList,
        'OrderDetails' => [$orderDetail]
    ];
    echo json_encode($object);

?>